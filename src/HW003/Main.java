package HW003;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    ArrayList<StaffMember> staffMembers = new ArrayList<>();

    void start(){
        do {
            menu();
        }while (true);
    }
    private void menu(){
        String inputOption;
        Scanner sc = new Scanner(System.in);
        show();
        System.out.println("\t 1). Add Employee \t 2). Edit \t 3). Remove \t 4). Exit");
        System.out.print("=> Choose option(1-4): ");
        inputOption = sc.nextLine();
        switch (Integer.parseInt(inputOption)) {
            case 1:
                addEmployee();
                break;
            case 2:
                edit();
                break;
            case 3:
                remove();
                break;
            case 4:
                exit();
                break;
        }
    }

    private void addEmployee(){
        String inputOption;
        Scanner sc = new Scanner(System.in);
        System.out.println("\t 1). Volunteer \t 2). Hourly Emp \t 3). Salaried Emp \t 4). Back");
        System.out.print("=> Choose option(1-4): ");
        inputOption = sc.nextLine();
        switch (Integer.parseInt(inputOption)) {
            case 1:
                addVolunteer();
                break;
            case 2:
                addHourlyEmp();
                break;
            case 3:
                addSalariedEmp();
                break;
            case 4:
                exit();
                break;
        }
    }
    private void addVolunteer(){
        String name, id, address;
        Scanner sc = new Scanner(System.in);

        System.out.println("============ INSERT INTO ============");
        System.out.print("\n=> Enter Staff Member's ID        : ");
        id = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Name      :  ");
        name = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Address : ");
        address = sc.nextLine();
        Volunteer volunteer = new Volunteer(Integer.parseInt(id), name, address);
        this.staffMembers.add(volunteer);
    }
    private void addHourlyEmp(){
        String name, id, address, hourworked, rate;

        Scanner sc = new Scanner(System.in);

        System.out.println("============ INSERT INTO ============");
        System.out.print("\n=> Enter Staff Member's ID         : ");
        id = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Name       :  ");
        name = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Address    : ");
        address = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's HourWorded :  ");
        hourworked = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Rate       : ");
        rate = sc.nextLine();
        HourEmployee employee = new HourEmployee(Integer.parseInt(id), name, address, Integer.parseInt(hourworked), Double.parseDouble(rate));
        this.staffMembers.add(employee);
    }

    private void addSalariedEmp(){
        String name, id, address, salary, bonus;

        Scanner sc = new Scanner(System.in);

        System.out.println("============ INSERT INTO ============");
        System.out.print("\n=> Enter Staff Member's ID       : ");
        id = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Name     :  ");
        name = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Address  : ");
        address = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Salary   :  ");
        salary = sc.nextLine();
        System.out.print("\n=> Enter Staff Member's Bonus    : ");
        bonus = sc.nextLine();
        SalaryEmployee employee = new SalaryEmployee(Integer.parseInt(id), name, address, Double.parseDouble(salary), Double.parseDouble(bonus));
        staffMembers.add(employee);
    }

    private void show(){
        for (StaffMember s:
                staffMembers) {
            System.out.println(s.toString());
        }
    }

    private void edit(){
        String inputID;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input ID : ");
        inputID = sc.next();
        StaffMember s = staffMembers.get(Integer.parseInt(inputID));
        if(s instanceof Volunteer){
            Volunteer v = (Volunteer)s;
            String id, name, address;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Edit ID : ");
            id = scanner.next();
            System.out.println("Edit Name : ");
            name = scanner.next();
            System.out.println("Edit Address : ");
            address=scanner.next();
            v.id = Integer.parseInt(id);
            v.name = name;
            v.address= address;
        }else if(s instanceof HourEmployee){
            HourEmployee h = (HourEmployee)s;
            String id, name, address;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Edit ID : ");
            id = scanner.next();
            System.out.println("Edit Name : ");
            name = scanner.next();
            System.out.println("Edit Address : ");
            address=scanner.next();
            h.id = Integer.parseInt(id);
            h.name = name;
            h.address= address;
        }else if(s instanceof SalaryEmployee){
            SalaryEmployee z = (SalaryEmployee)s;
            String id, name, address;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Edit ID : ");
            id = scanner.next();
            System.out.println("Edit Name : ");
            name = scanner.next();
            System.out.println("Edit Address : ");
            address=scanner.next();
            z.id = Integer.parseInt(id);
            z.name = name;
            z.address= address;
        }
    }
    private void remove(){
        String inputID;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input ID : ");
        inputID = scanner.next();
        StaffMember s = staffMembers.get(Integer.parseInt(inputID));
        staffMembers.remove(s);
    }
    private void exit(){
        System.exit(0);
    }

    public static void main(String[] args) {
        new Main().start();
    }
}
