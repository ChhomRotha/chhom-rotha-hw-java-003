package HW003;

public class SalaryEmployee extends StaffMember {
    private double salary;
    private double bonus;

    public SalaryEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "StaffMember" + "\nID: "+ id + "\n Name: " + name +"\n Address: "+ address +"\n Salary: " +salary + "\n bonus: " + bonus;
    }

    @Override
    public double pay() {
        return salary+bonus;
    }
}
