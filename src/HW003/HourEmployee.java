package HW003;

public class HourEmployee extends StaffMember {
    private int hourWorked;
    private double rate;
    public HourEmployee(int id, String name, String address, int hourWorked, double rate){
        super(id, name, address);
        this.hourWorked = hourWorked;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "HourEmployee" + "\nID: "+ id +"\n Name: " + name +"\n Address: "+ address + "\n Hourworked :" + hourWorked +"\n Rate: " +rate ;

    }

    @Override
    public double pay() {
        return hourWorked*rate;
    }
}
