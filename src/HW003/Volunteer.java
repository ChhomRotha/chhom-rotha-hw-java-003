package HW003;

public class Volunteer extends  StaffMember{
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }
    @Override
    public double pay() {
        return 0;
    }

    @Override
    public String toString() {
        return "Volunteer" + "\nID: "+ id + "\n Name: " + name +"\n Address: "+ address;

    }
}
