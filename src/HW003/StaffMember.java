package HW003;

public  abstract class StaffMember {
    protected int id;
    protected String name;
    protected String address;
    public StaffMember( int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String toString() {
        return "StaffMember" + "\nID: "+ id + "\n Name: " + name +"\n Address: "+ address;
    }

    public abstract double pay();

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

}
